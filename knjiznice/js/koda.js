$(document).ready(function() {
    document.getElementById("dodaj").addEventListener('click',dodajBolnika);
    document.getElementById("zemljevid").addEventListener('load',narisiZemljevid);
	narisiZemljevid();
});

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var map;
var username = "ois.seminar";
var password = "ois4fri";
var inputKraj = document.getElementById("kreirajKraj");

function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function narisiZemljevid(){
	map = new Microsoft.Maps.Map(document.getElementById('zemljevid'), {
		credentials: 'AgDLUbbPowa2Ny78CbdcQAm9LH1wYm7ba8pQXMRjYTakVI4C7iI8RsfLwc6PevfG'
	});
}

function najdiMe(){
	var kraj = $("#kreirajKraj").val();
	if(!kraj || kraj.trim().length == 0){
		$("#kreirajSporocilo1").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite iskani kraj!</span>");
	}else{
		console.log("ja");
		 map.FindLocation(document.getElementById('kreirajKraj').value);
	}
}

function dodajBolnika(){
    var sessionId = getSessionId();
    var ime = $("#kreirajIme").val();
    var skt = $("#kreirajSistolicniTlak").val();
    var dkt = $("#kreirajDiastolicniTlak").val();
    
    if (!ime || !skt|| !dkt  || ime.trim().length == 0 ||
      skt.trim().length == 0 || dkt.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            SKT: skt,
		            DKT: dkt,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


function generirajPodatke(stPacienta) {
  var ehrId = "";

  

  return ehrId;
}